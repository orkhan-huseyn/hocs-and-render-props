import React from 'react';
import './styles.css';

class Panel extends React.Component {
  render() {
    return <div className="panel">{this.props.children}</div>;
  }
}

export default Panel;
