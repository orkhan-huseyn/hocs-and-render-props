import React from 'react';

function withNetwork(WrappedComponent) {
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isOnline: navigator.onLine,
      };
      this.handleNetworkChange = this.handleNetworkChange.bind(this);
    }

    handleNetworkChange(event) {
      this.setState({
        isOnline: event.type === 'online',
      });
    }

    componentDidMount() {
      window.addEventListener('online', this.handleNetworkChange);
      window.addEventListener('offline', this.handleNetworkChange);
    }

    componentWillUnmount() {
      window.removeEventListener('online', this.handleNetworkChange);
      window.removeEventListener('offline', this.handleNetworkChange);
    }

    render() {
      return <WrappedComponent isOnline={this.state.isOnline} />;
    }
  };
}

export default withNetwork;
