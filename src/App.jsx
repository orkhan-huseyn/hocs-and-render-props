import React from 'react';
import MouseTracker from './components/MouseTracker';
import Panel from './components/Panel';
import withNetwork from './hocs/withNetwork';

class App extends React.Component {
  render() {
    const { isOnline } = this.props;
    return (
      <>
        <span style={{ color: isOnline ? 'green' : 'red' }}>
          {isOnline ? 'You are online' : 'You are offline'}
        </span>
        <Panel>
          <h1>Hello from Panel!</h1>
          <button>Click me!</button>
        </Panel>
        <MouseTracker>
          {({ x, y }) => {
            return (
              <h1>
                hello from custom content {x} and {y}
              </h1>
            );
          }}
        </MouseTracker>
      </>
    );
  }
}

export default withNetwork(App);
